(*
//
** Midterm
//
** Course: BU CAS CS520
//
** Out: noon on the 26th of October
** Due: 11:59pm on the 26th of October
//
*)

(* ****** ****** *)
//
#include
"share/atspre_define.hats"
#include
"{$LIBATSCC2JS}/staloadall.hats"
//
(* ****** ****** *)
//
datatype
intlist(int) =
| intlist_nil(0) of ()
| {x:int}{xs:int}
  intlist_cons(xs+x) of (int(x), intlist(xs))
//
(* ****** ****** *)
//
// HX: 10 points
//
(*
typedef Int = [i:int] int(i)
*)
//
// Note that [sublist(xs, m)] returns a stream
// of all the subsequences of [xs] such that the
// sum of each subsequence equals m. For instance,
// let xs be 1, 1, 2, 3, and 4; sublist(xs, 4) should
// be a stream consisting of the following subsequences:
// (1, 1, 2), (1, 3), (1, 3), and (4).
//
extern
fun
sublist_sum
  {m:int}
  (xs: list0(Int), m: int(m)): stream(intlist(m))
//
(* ****** ****** *)

(* end of [Q5.dats] *)
