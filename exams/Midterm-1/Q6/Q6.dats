(*
//
** Midterm
//
** Course: BU CAS CS520
//
** Out: noon on the 26th of October
** Due: 11:59pm on the 26th of October
//
*)

(* ****** ****** *)

(*
**
** Assume that ABCD is a quadrilateral
** such that
** 1. A and D are to the west of B and D, respectively
** 2. A and B are to the north of D and C, respectively
**
** Let M and N be two points between AB and CD, respectively.
**
** Let P be the intersection of two diagonals of AMND
** Let Q be the intersection of two diagonals of ABCD
** Let R be the intersection of two diagonals of MBCN
**
** Prove that P, Q and R are colinear.
**
** Hint:
** You may assume that C and D are on the x-axis, that is,
** the y-coordinates of C and D are 0.
**
*)

(* ****** ****** *)
//
// HX: 20 points
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
//
(* ****** ****** *)
//
staload
"libats/SATS/Number/real.sats"
//
(* ****** ****** *)

stacst x1: real
stacst y1: real // A: (x1, y1)
stacst x2: real
stacst y2: real // B: (x2, y2)
stacst x3: real
stacst y3: real // C: (x3, y3)
stacst x4: real
stacst y4: real // D: (x4, y4)

(* ****** ****** *)
//
stadef
colinear
(
  xa: real, ya: real
, xb: real, yb: real
, xc: real, yc: real
) : bool =
  (xa-xb)*(yb-yc) == (xb-xc)*(ya-yb)
//
(* ****** ****** *)
//
// Please find a way to encode in ATS the
// above stated theorem and then prove the
// encoded statement (via patsolve_z3).
//
(* ****** ****** *)

(* end of [Q6.dats] *)
