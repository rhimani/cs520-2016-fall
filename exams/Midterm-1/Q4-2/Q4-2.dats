(* ****** ****** *)
//
typedef
mymat(a:t@ype, m:int, n:int) = list(list(a, n), m)
//
// mymat uses row-major representation for matrices;
// each matrix is represented by a list of lists, where
// each list element represents a row in the matrix.
//
(* ****** ****** *)

typedef T = double

(* ****** ****** *)
//
// HX: 10 points
// lease implement matrix transposition
//
extern
fun
mymat_transpose
  {m,n:nat}(mymat(T, m, n)): mymat(T, n, m)
//
(* ****** ****** *)
//
// HX: 10 points
// please implement matrix multiplication
//
extern
fun
mul_mymat_mymat{p,q,r:nat}
  (M1: mymat(T, p, q), M2: mymat(T, q, r)): mymat(T, p, r)
//
(* ****** ****** *)

(* end of [Q4-2.dats] *)
