(*
** Enumerate the words
** according to their frequencies
** in a given file.
*)

(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
//
%{^
//
#undef ATSextfcall
#define ATSextfcall(fun, arg) fun arg
//
%} // ...
//
staload
"libats/libc/SATS/unistd.sats"
staload
"libats/libc/SATS/sys/wait.sats"
//
(* ****** ****** *)
//
staload
UN =
"prelude/SATS/unsafe.sats"
//
(* ****** ****** *)

staload
"./../../../mylib/mystream.dats"

(* ****** ****** *)
//
#define WordCounting 1
//
staload "./WordCounting.sats"
//
(* ****** ****** *)

local
#include "./WordCounting_lib.dats"
in (*nothing*) end // end of [local]

(* ****** ****** *)
//
val
theStream = ref<ptr>(the_null_ptr)
//
(* ****** ****** *)

implement
char_get() = let
  val p0 = !theStream
in
//
if
isneqz(p0)
then let
//
val cs =
$UN.castvwtp0{stream_vt(char)}(p0)
//
in
  case+ !cs of
  | ~stream_vt_nil() =>
    (!theStream := the_null_ptr; ~1)
  | ~stream_vt_cons(c, cs) =>
    (!theStream := $UN.castvwtp0(cs); char2u2int0(c))
end // end of [then]
else (~1) // end of [else]
//
end // end of [char_get]

(* ****** ****** *)

#define N 250

implement
main(
 argc, argv
) = 0 where {
//
val cs =
(
if
argc >= 2
then stream_by_url(argv[1])
else streamize_fileref_char(stdin_ref)
) : stream_vt(char)
//
val () = !theStream := $UN.castvwtp0(cs)
//
fun aux
(
  i: int, nws: list0(intstr)
) : void =
(
case+ nws of
| list0_nil() => ()
| list0_cons(nw, nws) =>
    if i < N then
      (println! (i+1, ": ", nw.1, ": ", nw.0); aux(i+1, nws))
    // end of [if]
)
//
val () = aux(0, WordCounting_main())
//
} (* end of [main] *)
//
(* ****** ****** *)

(* end of [WordCounting.dats] *)
