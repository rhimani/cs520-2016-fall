(*
** Enumerate the words
** according to their frequencies
** in a given file.
*)

(* ****** ****** *)
//
#include
"share/atspre_define.hats"
#include
"{$LIBATSCC2PY3}/staloadall.hats"
//
(* ****** ****** *)

#define WordCounting 1

(* ****** ****** *)
//
#define
ATS_EXTERN_PREFIX "WordCounting_"
#define
ATS_STATIC_PREFIX "_WordCounting_"
//
(* ****** ****** *)
//
#define ATS_MAINATSFLAG 1
#define ATS_DYNLOADNAME "WordCounting_dynload"
//
(* ****** ****** *)
//
#staload "./WordCounting.sats"
//
(* ****** ****** *)
//
local
#include "./WordCounting_lib.dats"
in (*nothing*) end
//
(* ****** ****** *)
//
implement
char_get() = $extfcall(int, "char_get")
//
(* ****** ****** *)

%{^
######
import sys
######
from libatscc2py_all import *
######
sys.setrecursionlimit(1000000)
######
%} // end of [%{^]

(* ****** ****** *)
//
staload
UN =
"prelude/SATS/unsafe.sats"
//
(* ****** ****** *)
//
implement
gprint_newline<>
(
// argmentless
) = gprint_string<>("\n")
//
(* ****** ****** *)
//
extern
fun
WordCounting_main_
  (status: int, data: string): string = "mac#"
//
implement
WordCounting_main_
(
  status, data
) = let
//
val out =
  PYlist_nil{string}()
val ((*void*)) =
  $extfcall(void, "theSource_initize", data)
//
implement
gprint_string<>(x) = PYlist_append(out, x)
//
val nxs =
  WordCounting_main()
// end of [val]
val nxs = g1ofg0(nxs)
val nxs =
(
  if length(nxs) <= 250 then nxs else list_take(nxs, 250)
) : List0(intstr) // end of [val]
//
fun
loop
(
  i: int, nxs: List(intstr)
) : void =
(
case+ nxs of
| list_nil() => ()
| list_cons(nx, nxs) =>
  loop(i+1, nxs) where
  {
    val () = gprintln!(i, ": ", nx.1, "(", nx.0, ")", "<br>")
  } (* end of [list_cons] *)
)
//
val () = gprintln! ("<html>")
val () = gprintln! ("<head>")
val () = gprintln! ("</head>")
val () = gprintln! ("<body>")
val () = gprintln! ("<h1>WordCounting(result)</h1>")
val () = gprintln! ("<pre>")
val () = loop(1, nxs)
val () = gprintln! ("</pre>")
val () = gprintln! ("<h1>WordCounting(source)</h1>")
val () = gprintln! ("<pre>", data, "</pre>")
val () = gprintln! ("</body>")
val () = gprintln! ("</html>")
//
in
  PYlist_string_join(out)
end // end of [WordCounting_main_]
//
(* ****** ****** *)

%{$

################################################

import urllib.error
import urllib.request

################################################

def WordCounting_main_url(url):
  data = ""
  status = -1
  try:
    resp = urllib.request.urlopen(url)
    data = resp.read().decode('utf-8')
    status = resp.status
  except ValueError as resp_exn:
    data = str(resp_exn)
  except urllib.error.URLError as resp_exn:
    data = str(resp_exn)
  WordCounting_dynload()
  return WordCounting_main_(status, data)

################################################

theSource = {
  'data': '', 'pos': 0, 'tot': 0
} #end-of-theSource

def char_get():
  pos = theSource['pos']
  if (pos < theSource['tot']):
    res = theSource['data'][pos]
    theSource['pos'] = pos+1; return ord(res)
  else:
    return -1
  #endif

################################################

def theSource_initize(data):
  theSource['data'] = data; theSource['pos'] = 0; theSource['tot'] = len(data); return

################################################

#
# Moby Dick:
# http:||www.gutenberg.org/files/2701/2701.txt
#

if __name__ == '__main__':
  if len(sys.argv) <= 1:
    data = sys.__stdin__.read(-1);
    WordCounting_dynload()
    print(WordCounting_main_(0, data))
  #endif
  if len(sys.argv) >= 2:
    print(WordCounting_main_url(sys.argv[1]))
  #endif
#endif

################################################

%} // end of [%{$]

(* ****** ****** *)

(* end of [WordCounting.dats] *)
