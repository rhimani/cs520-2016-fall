(*
** Dependent types
*)

(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
//
(* ****** ****** *)

extern
fun
fact :
{i:nat} int(i) -> [r:int] int(r)

(* ****** ****** *)

implement
fact(x) =
if x > 0 then x * fact(x-1) else 1

(* ****** ****** *)
//
val
fact10 = fact(10)
//
// val
// fact_1 = fact(~1) // type-error
//
val
factfact10 = let
  val fact10 = fact(10)
  val ((*void*)) = assertloc (fact10 >= 0)
in
  fact(fact10)
end // end of [val]
//
(* ****** ****** *)

extern
fun
{a:t0ype}
mylist_length : {n:nat} list(a, n) -> int(n)

(* ****** ****** *)

implement
{a}(*tmp*)
mylist_length(xs) = let
//
fun
loop{i,j:nat}(xs: list(a, i), j: int(j)): int(i+j) =
(
case+ xs of
| list_nil() => j
| list_cons(_, xs) => loop(xs, j+1)
)
//
in
  loop(xs, 0)
end (* end of [mylist_length] *)

(* ****** ****** *)

(* end of [deptype.dats] *)
