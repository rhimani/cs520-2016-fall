(*
** Listing all the
** permutations of a given list
*)

(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"

(* ****** ****** *)
//
stacst fact : int -> int
//
extern
prfun
lemma_fact_0 :
  () -<prf> [fact(0)==1] void
extern
prfun
lemma_fact_pos :
  {n:pos} () -<prf> [fact(n)==n*fact(n-1)] void
//
(* ****** ****** *)
//
// HX: 20 points
// Please implement permute1
// directly based on permute0
//
extern
fun{
a:t0ype
} permute1
  {n:nat}
(
  xs: list(INV(a), n)
) : [r:int] list(list(a, n), r)
//
(* ****** ****** *)
//
// HX: 20 bonus points
// Please implement permute2
// directly based on permute0
//
extern
fun{
a:t0ype
} permute2
  {n:nat}
  (xs: list(INV(a), n)): list(list(a, n), fact(n))
//
(* ****** ****** *)

(* end of [permute12.dats] *)
