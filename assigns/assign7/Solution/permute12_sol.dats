(*
** Listing all the
** permutations of a given list
*)

(* ****** ****** *)

#include "./../permute12.dats"

(* ****** ****** *)
//
// Please give
// your implementation as follows:
//
(* ****** ****** *)
//
extern
fun
{a:t0p}
choose1{n:int | n >= 1}
(
xs: list(INV(a), n)
) : list($tup(a, list(a, n-1)), n)
//
implement
{a}(*tmp*)
choose1
  {n}(xs) =
(
case+ xs of
| cons(x0, xs) =>
  (
  case+ xs of
  | nil() => cons($tup(x0, nil()), nil())
  | cons _ =>
    cons
    ( $tup(x0, xs)
    , list_vt2t
      (
        list_map_cloref<$tup(a,list(a,n-2))><$tup(a,list(a,n-1))>
          (choose1(xs), lam($tup(x1, xs)) => $tup(x1, cons(x0, xs)))
      ) (* list_vt2t *)
    )
  ) (* end of [cons] *)
)
//
(* ****** ****** *)
//
extern
fun
{a:t@ype}
mylist_concat
  (List(List(INV(a)))): List(a)
extern
fun
{a:t@ype}
mylist_mapcons{m,n:int}
  (a, list(list(INV(a), n), m)): list(list(a, n+1), m)
extern
fun
{a:t@ype}
{b:t@ype}
mylist_map_cloref
  {n:int}(list(INV(a), n), (a) -<cloref1> b): list(b, n)
//
(* ****** ****** *)

implement
{a}(*tmp*)
permute1
  {n}(xs) =
(
case+ xs of
| nil() =>
  list_sing(nil())
| cons _ =>
  mylist_concat<list(a, n)>
  (
    mylist_map_cloref<$tup(a, list(a, n-1))><List(list(a, n))>
      (choose1(xs), lam($tup(x, xs)) => mylist_mapcons(x, permute1(xs)))
  )
)
//
(* ****** ****** *)
//
extern
fun
{a:t@ype}
mylist_concat2
  {m,n:int}
  (list(list(INV(a), n), m)): list(a, m*n)
//
(* ****** ****** *)

implement
{a}(*tmp*)
permute2
  {n}(xs) = let
in
//
case+ xs of
| nil() => let
    prval () = lemma_fact_0()
  in
    list_sing(nil())
  end // end of [nil]
| cons _ => let
    prval () = lemma_fact_pos{n}()
  in
    mylist_concat2
    (
      mylist_map_cloref<$tup(a,list(a,n-1))><list(list(a,n),fact(n-1))>
        (choose1(xs), lam($tup(x, xs)) => mylist_mapcons(x, permute2(xs)))
    )
  end // end of [cons]
//
end // end of [permute2]
//
(* ****** ****** *)

(* end of [permute12_sol.dats] *)
