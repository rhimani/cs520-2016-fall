(*
//
// Please implement
// fib_cassini in ATS
//
*)
(* ****** ****** *)
//
// HX: 20 points
//
(* ****** ****** *)
//
// SGN (n, i) : i = (-1)^n
//
dataprop
SGN (int, int) =
  | SGNbas (0, 1)
  | {n:nat} {i:int} SGNind (n+1, ~i) of SGN (n, i)
// end of [SGN]
//
(* ****** ****** *)
//
// HX: the defintion of fib function:
// fib(0) = 0; fib(1) = 1; fib(n+2)=fib(n)+fib(n+1)
//
dataprop
FIB (int, int) =
  | FIBbas0 (0, 0) of ()
  | FIBbas1 (1, 1) of ()
  | {n:nat}{r0,r1:int}
    FIBind2 (n+2, r0+r1) of (FIB (n, r0), FIB (n+1, r1))
// end of [FIB]
//
(* ****** ****** *)
//
// Cassini's formula states:
// fib(n)*fib(n+2) + (-1)^n = (fib(n+1))^2
//
extern
prfun
fib_cassini
{n:nat}
{f0,f1,f2:int}
{i:int}
(
  pf0: FIB (n, f0)
, pf1: FIB (n+1, f1)
, pf2: FIB (n+2, f2)
, pf3: SGN (n, i)
) : [f0*f2 + i == f1*f1] void

(* ****** ****** *)

(* end of [fib_cassini.dats] *)
