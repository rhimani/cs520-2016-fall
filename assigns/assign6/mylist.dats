//
// Title: Principles of Programming Languages
// Number: CAS CS 520
// Semester: Fall 2016
// Class Time: MW 2:30-4:00
// Instructor: Hongwei Xi (hwxiATcsDOTbuDOTedu)
//
(* ****** ****** *)
//
// It is due Monday, the 26th of October
//
(* ****** ****** *)

#define
ATS_PACKNAME "CS520-FALL-2016"

(* ****** ****** *)
//
datatype
mylist(a:t@ype+, int) =
//
  | mylist$nil(a, 0) of ()
//
  | {n:nat}
    mylist$cons(a, n+1) of (a, mylist(a, n))
  | {n:nat}
    mylist$snoc(a, n+1) of (int(n), mylist(a, n), a)
//
  | {n1,n2:pos}
    mylist$append(a, n1+n2) of
      (int(n1), int(n2), mylist(a, n1), mylist(a, n2))
    // end of [mylist$append]
//
  | {n:pos}
    mylist$reverse(a, n) of (int(n), mylist(a, n))
//
(* ****** ****** *)
//
// HX: given
//
extern
fun
{a:t@ype}
mylist_isnil{n:int}(mylist(INV(a), n)): bool(n==0)
//
(* ****** ****** *)
//
implement
{a}(*tmp*)
mylist_isnil(xs) =
(
case+ xs of
//
| mylist$nil() => true
| _(*non-mylist$nil*) =>> false
//
) (* end of [mylist_isnil] *)
//
(* ****** ****** *)
//
// HX: 10 points
//
extern
fun
{a:t@ype}
mylist_length{n:int}(mylist(INV(a), n)): int(n)
//
(* ****** ****** *)
//
// HX: 10 points
//
extern
fun
{a:t@ype}
mylist_get_at
  {n:int}
  {i:nat | i < n}(mylist(INV(a), n), int(i)): (a)
//
overload [] with mylist_get_at
//
(* ****** ****** *)
//
// HX: 10 points
//
extern
fun
{a:t@ype}
mylist_listize{n:int}(mylist(INV(a), n)): list(a, n)
//
(* ****** ****** *)

(* end of [mylist.dats] *)
