(*
** Assignment 4
** It is due Wednesday, the 5th of October, 2016
*)

(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)

#include "./../assign4.dats"

(* ****** ****** *)
//
extern
fun{a:t@ype}
stream_take_until
  (xs: &stream(INV(a)) >> _, (a) -<cloref1> bool): list0(a)
//
(* ****** ****** *)
//
extern
fun{a:t@ype}
stream_vt_take_until
  (xs: &stream_vt(INV(a)) >> _, (a) -<cloref1> bool): list0(a)
//
(* ****** ****** *)

implement
{a}(*tmp*)
stream_take_until
  (xs0, pred) = let
//
fun
loop
(
  xs0: &stream(a) >> _, res: list0(a)
) : list0(a) =
(
  case+ !xs0 of
  | stream_nil() => res
  | stream_cons(c, xs1) =>
    if pred(c)
      then res else (xs0 := xs1; loop(xs0, cons0(c, res)))
    // end of [if]
) (* end of [loop] *)
//
in
  loop(xs0, list0_nil())
end // end of [stream_take_until]

(* ****** ****** *)

implement
{a}(*tmp*)
stream_vt_take_until
  (xs0, pred) = let
//
fun
loop
(
  xs0: &stream_vt(a) >> _, res: list0(a)
) : list0(a) = let
  val xs0_ = !xs0
in
  case+ xs0_ of
  | ~stream_vt_nil() =>
    (xs0 := stream_vt_make_nil(); res)
  | @stream_vt_cons(c, xs1) =>
    if pred(c)
      then let
        prval () = fold@(xs0_)
      in
        xs0 := stream_vt_make_con<a>(xs0_); res
      end // end of [then]
      else let
        val c = c and xs1 = xs1
      in
        free@(xs0_); xs0 := xs1; loop(xs0, cons0(c, res))
      end // end of [else]
    // end of [if]
end (* end of [loop] *)
//
in
  loop(xs0, list0_nil())
end // end of [stream_vt_take_until]

(* ****** ****** *)

implement
stream_wordizing
  (cs0) = $delay(
//
let
//
  var cs0 = cs0
//
  val cs1 =
    stream_take_until(cs0, lam(c) => isalpha(c))
  // end of [val]
//
  val cs2 = 
    stream_take_until(cs0, lam(c) => ~isalpha(c))
  // end of [val]
//
in
  case+ cs2 of
  | list0_nil _ => stream_nil()
  | list0_cons _ => stream_cons(string_make_rlist(cs2), stream_wordizing(cs0))
end // end of [let]
//
) (* end of [stream_wordizing] *)

(* ****** ****** *)

implement
stream_vt_wordizing
  (cs0) = $ldelay
(
//
let
//
  var cs0 = cs0
//
  val cs1 =
    stream_vt_take_until(cs0, lam(c) => isalpha(c))
  // end of [val]
//
  val cs2 = 
    stream_vt_take_until(cs0, lam(c) => ~isalpha(c))
  // end of [val]
//
  val cs0 = cs0
//
in
  case+ cs2 of
  | list0_nil _ => (~cs0; stream_vt_nil())
  | list0_cons _ => stream_vt_cons(string_make_rlist(cs2), stream_vt_wordizing(cs0))
end : stream_vt_con(word)
//
, (~cs0) // called when the stream is freed
//
) (* end of [stream_vt_wordizing] *)

(* ****** ****** *)

implement
main0() = () where
{
//
val () = assign4_test()
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [assign4_sol.dats] *)
