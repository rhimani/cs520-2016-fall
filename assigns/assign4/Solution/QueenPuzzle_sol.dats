(*
** Solving the 8-queen puzzle
*)

(* ****** ****** *)

#include "./../QueenPuzzle.dats"

(* ****** ****** *)
//
#define N 8 // it can be changed
//
(* ****** ****** *)
//
(*
implement
QueenPuzzle_solve() =
((fix qsolve(n: int): stream(list0(int)) => if(n > 0)then((qsolve(n-1)*list0_make_intrange(0,N)).map(TYPE{list0(int)})(lam($tup(xs,x))=>cons0(x,xs))).filter()(lam(xs)=>let val-cons0(x0,xs) = xs in xs.iforall()(lam(i, x)=>((x0)!=x)&&(abs(x0-x)!=i+1)) end)else(stream_make_sing(nil0())))(N)).map(TYPE{list0(int)})(lam(xs) => list0_reverse(xs))
*)
//
(* ****** ****** *)
//
fun
safety_test
(
  x0: int, xs: list0(int)
) : bool =
  xs.iforall()(lam(i, x) => ((x0)!=x)&&(abs(x0-x)!=i+1))
//
(* ****** ****** *)
//
fun
psolution_extend
(
  xs: list0(int)
) : stream(list0(int)) =
(
  (N).stream_map(TYPE{list0(int)})(lam x => list0_cons(x, xs))
).filter()(lam(xs) => let val-cons0(x, xs) = xs in safety_test(x, xs) end)
//
(* ****** ****** *)

(*
implement
QueenPuzzle_solve() = let
//
fun
aux(n: int): stream(list0(int)) =
  if n > 0
    then stream_concat((aux(n-1)).map(TYPE{stream(list0(int))})(lam(xs) => psolution_extend(xs)))
    else stream_make_sing(nil0())
  // end of [if]
//
in
  (aux(N)).map(TYPE{list0(int)})(lam(xs) => reverse(xs))
end (* end of [QueenPuzzle_solve] *)
*)

(* ****** ****** *)
//
fun
solve_succ
(
  xss: stream(list0(int))
) : stream(list0(int)) =
  stream_concat(xss.map(TYPE{stream(list0(int))})(lam xs => psolution_extend(xs)))
//
(* ****** ****** *)
//
implement
QueenPuzzle_solve() = (
(
fix
f0(
  n: int, xss: stream(list0(int))
) : stream(list0(int)) =>
  if n > 0 then f0(n-1, solve_succ(xss)) else xss
)(N, stream_make_sing(nil0))
).map(TYPE{list0(int)})(lam xs => list0_reverse(xs))
//
(* ****** ****** *)

implement
QueenPuzzle_solve2() = let
//
fun
aux0
(
  nt0: int
, xss: stream(list0(int))
) : stream(list0(int)) =
  if nt0 < N
    then aux0(nt0+1, aux1(nt0, xss)) else xss
  // end of [if]
//
and
aux1
(
  nt0: int, xss: stream(list0(int))
) : stream(list0(int)) = $delay
(
case+ !xss of
| stream_nil() =>
    stream_nil((*void*))
| stream_cons(xs, xss) =>
    if length(xs) < nt0
      then stream_cons(xs, aux1(nt0, xss))
      else stream_cons(xs, aux2(0, xs, nt0, xss))
    // end of [if]
) (* end of [aux1] *)
//
and
aux2
(
  x: int, xs: list0(int)
, nt0: int, xss: stream(list0(int))
) : stream(list0(int)) = $delay
(
//
if
(x < N)
then let
//
val ok =
  safety_test(x, xs)
//
in
//
if
(ok)
then
stream_cons
(
  list0_cons(x, xs)
, aux2(x+1, xs, nt0, xss)
) (* stream_cons *)
else !(aux2(x+1, xs, nt0, xss))
end // end of [then]
else !(aux1(nt0, xss))
//
) (* end of [aux2] *)
//
in
  stream_map_cloref(aux0(0, stream_make_sing(list0_nil)), lam(xs) => list0_reverse(xs))
end // end of [QueenPuzzle_solve2]

(* ****** ****** *)

(* end of [QueenPuzzle_sol.dats] *)
