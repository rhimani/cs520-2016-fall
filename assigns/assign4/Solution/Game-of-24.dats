

typedef int2 = $tup(int, int)

fun
choose2(list0(int)): stream(list0(int2))


fun
myopr (int, int): list0(int)

fun
solve_one(xs: list0(int)): stream(list0(int)) =
  val xys = choose2(xs)
  val ... = xys.map(...)

////
fun
Game_solve(xs: list0(int)): bool = 