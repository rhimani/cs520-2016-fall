(*
** Solving the 8-queen puzzle
*)

(* ****** ****** *)

#include "./../QueenPuzzle.dats"

(* ****** ****** *)
//
// HX:
// Testing whether adding an
// additional queen piece is okay
//
extern
fun
safety_test
  (x0: int, xs: list0(int)): bool
//
(* ****** ****** *)
//
// HX:
// Extending a partial
// solution with one more piece
//
extern
fun
psolution_extend :
  list0(int) -> stream(list0(int))
//
(* ****** ****** *)
//
fun
solve_succ
(
  xss: stream(list0(int))
) : stream(list0(int)) =
  stream_concat(xss.map(TYPE{stream(list0(int))})(lam xs => psolution_extend(xs)))
//
(* ****** ****** *)
//
implement
QueenPuzzle_solve() = (
(
fix
f0(
  n: int, xss: stream(list0(int))
) : stream(list0(int)) =>
  if n > 0 then f0(n-1, solve_succ(xss)) else xss
)(N, stream_make_sing(nil0))
).map(TYPE{list0(int)})(lam xs => list0_reverse(xs))
//
(* ****** ****** *)

(* end of [QueenPuzzle_hint.dats] *)
