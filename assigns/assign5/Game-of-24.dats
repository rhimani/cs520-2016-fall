(*
** Game-of-24
*)

(* ****** ****** *)
//
#include
"share/atspre_define.hats"
#include
"{$LIBATSCC2JS}/staloadall.hats"
//
(* ****** ****** *)
//
#define
ATS_MAINATSFLAG 1
#define
ATS_DYNLOADNAME "Game24_dynload"
//
#define
ATS_STATIC_PREFIX "_Game_of_24_"
//
(* ****** ****** *)
//
extern
fun
{a:t0p}
choose1
(
  xs: list0(a)
) : stream($tup(a, list0(a)))
extern
fun
{a:t0p}
choose2
(
  xs: list0(a)
) : stream($tup(a, a, list0(a)))
//
(* ****** ****** *)
//
implement
{a}(*tmp*)
choose1(xs) = $delay
(
case+ xs of
| nil0() =>
  stream_nil()
| cons0(x0, xs) =>
  stream_cons(
    $tup(x0, xs)
  , (choose1(xs)).map(TYPE{$tup(a, list0(a))})(lam($tup(x1, xs)) => $tup(x1, cons0(x0, xs)))
  )(* stream_cons *)
)
//
implement
{a}(*tmp*)
choose2(xs) =
(
//
case+ xs of
| cons0(x0, xs) =>
  stream_append
  (
    (choose1(xs)).map(TYPE{$tup(a, a, list0(a))})(lam($tup(x1, xs)) => $tup(x1, x0, xs))
  , (choose2(xs)).map(TYPE{$tup(a, a, list0(a))})(lam($tup(x1, x2, xs)) => $tup(x1, x2, cons0(x0, xs)))
  )
| list0_nil((*void*)) => stream_make_nil()
//
) (* end of [choose2] *)
//
(* ****** ****** *)

typedef dbl = double

(* ****** ****** *)

#define EPSILON 0.000001

(* ****** ****** *)

fun is_0(x: dbl) = abs(x) < EPSILON
fun is_24(x: dbl) = abs(x-24.0) < EPSILON

(* ****** ****** *)
//
extern
fun
arithops(x: dbl, y: dbl): list0(dbl)
//
(* ****** ****** *)
//
implement
arithops(x, y) =
  reverse(res) where
{
  val res = nil0()
  val res = cons0(x + y, res)
  val res = cons0(x - y, res)
  val res = cons0(y - x, res)
  val res = cons0(x * y, res)
  val res = (if is_0(y) then res else cons0(x / y, res)): list0(dbl)
  val res = (if is_0(x) then res else cons0(y / x, res)): list0(dbl)
}
//
(* ****** ****** *)
//
extern
fun
solve_one(list0(dbl)): stream(list0(dbl))
extern
fun
solve_ones(stream(list0(dbl))): stream(list0(dbl))
//
(* ****** ****** *)
//
implement
solve_one
  (xs) = let
//
val
x_y_zs_list =
  choose2(xs)
//
in
//
stream_concat
(
x_y_zs_list.map
(
TYPE
{
stream(list0(dbl))
}(*TYPE*)
)
(
lam
(
  $tup(x, y, zs)
) =>
  stream_make_list0
  (
    (arithops(x, y)).map(TYPE{list0(dbl)})(lam z => cons0(z, zs))
  )(* stream_make_list0 *)
)(* x_y_zs_list.map *)
)
//
end // end of [solve_one]
//
(* ****** ****** *)
//
implement
solve_ones =
lam(xss) =>
stream_concat(
  (xss).map(TYPE{stream(list0(dbl))})(lam xs => solve_one(xs))
)(* end of [solve_ones] *)
//
(* ****** ****** *)
//
extern
fun
Game24_play
(
  n1: int, n2: int, n3: int, n4: int
) : bool = "mac#"
//
(* ****** ****** *)

macdef i2d = int2double

(* ****** ****** *)

implement
Game24_play
(
  n1, n2, n3, n4
) = let
//
  val xs = nil0()
  val xs = cons0(i2d(n1), xs)
  val xs = cons0(i2d(n2), xs)
  val xs = cons0(i2d(n3), xs)
  val xs = cons0(i2d(n4), xs)
//
  val xss = solve_one(xs)
  val xss = solve_ones(xss)
  val xss = solve_ones(xss)
in
  xss.exists()(lam(xs) => let val-sing0(x) = xs in is_24(x) end)
end // end of [Game24_play]

(* ****** ****** *)

%{$
//
Game24_dynload();
//
alert("Game24(3, 3, 8, 8) = " + String(Game24_play(3, 3, 8, 8)))
alert("Game24(1, 3, 7, 11) = " + String(Game24_play(1, 3, 7, 11)))
alert("Game24(3, 5, 7, 13) = " + String(Game24_play(3, 5, 7, 13)))
alert("Game24(4, 4, 10, 10) = " + String(Game24_play(4, 4, 10, 10)))
alert("Game24(5, 5, 7, 11) = " + String(Game24_play(5, 5, 7, 11)))
alert("Game24(5, 7, 7, 11) = " + String(Game24_play(5, 7, 7, 11)))
//
%} // end of [%{$]

(* ****** ****** *)


(* end of [Game-of-24.dats] *)
