(*
** Assignment 5
** It is due Wednesday, the 12th of October, 2016
*)
(* ****** ****** *)

#include "./../assign5.dats"

(* ****** ****** *)

implement
{a}(*tmp*)
mylist_append
  (xs, ys) = let
//
fun
aux{m,n:nat}
(
  xs: list(a, m)
, ys: list(a, n)
) : list(a, m+n) =
(
  case+ xs of
  | list_nil() => ys
  | list_cons(x, xs) => list_cons(x, aux(xs, ys))
)
//
prval () = lemma_list_param(xs)
prval () = lemma_list_param(ys)
//
in
  aux(xs, ys)
end // end of [mylist_append]

(* ****** ****** *)

implement
{a}(*tmp*)
mylist_revappend
  (xs, ys) = let
//
fun
aux{m,n:nat}
(
  xs: list(a, m)
, ys: list(a, n)
) : list(a, m+n) =
(
  case+ xs of
  | list_nil() => ys
  | list_cons(x, xs) => aux(xs, list_cons(x, ys))
)
//
prval () = lemma_list_param(xs)
prval () = lemma_list_param(ys)
//
in
  aux(xs, ys)
end // end of [mylist_append]

(* ****** ****** *)

implement
{a,b}(*tmp*)
mylist_zip(xs, ys) = let
//
fun
aux{n:nat}
(
  xs: list(a, n)
, ys: list(b, n)
) : list((a, b), n) =
(
  case+ (xs, ys) of
  | (list_nil(), list_nil()) => list_nil()
  | (list_cons(x, xs), list_cons(y, ys)) => list_cons((x, y), aux(xs, ys))
)
//
prval () = lemma_list_param(xs)
//
in
  aux(xs, ys)
end // end of [mylist_zip]

(* ****** ****** *)

(* end of [assign5_sol.dats] *)

