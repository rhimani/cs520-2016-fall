(*
** Game-of-24
*)

(* ****** ****** *)
//
#include
"share/atspre_define.hats"
#include
"{$LIBATSCC2JS}/staloadall.hats"
//
(* ****** ****** *)
//
#define
ATS_MAINATSFLAG 1
#define
ATS_DYNLOADNAME "Game24_dynload"
//
#define
ATS_STATIC_PREFIX "_Game_of_24_"
//
(* ****** ****** *)
//
extern
fun
{a:t0p}
choose1
(
  xs: list0(a)
) : stream($tup(a, list0(a)))
extern
fun
{a:t0p}
choose2
(
  xs: list0(a)
) : stream($tup(a, a, list0(a)))
//
(* ****** ****** *)
//
implement
{a}(*tmp*)
choose1(xs) = $delay
(
case+ xs of
| nil0() =>
  stream_nil()
| cons0(x0, xs) =>
  stream_cons(
    $tup(x0, xs)
  , (choose1(xs)).map(TYPE{$tup(a, list0(a))})(lam($tup(x1, xs)) => $tup(x1, cons0(x0, xs)))
  )(* stream_cons *)
)
//
implement
{a}(*tmp*)
choose2(xs) =
(
//
case+ xs of
| cons0(x0, xs) =>
  stream_append
  (
    (choose1(xs)).map(TYPE{$tup(a, a, list0(a))})(lam($tup(x1, xs)) => $tup(x1, x0, xs))
  , (choose2(xs)).map(TYPE{$tup(a, a, list0(a))})(lam($tup(x1, x2, xs)) => $tup(x1, x2, cons0(x0, xs)))
  )
| list0_nil((*void*)) => stream_make_nil()
//
) (* end of [choose2] *)
//
(* ****** ****** *)

(*
typedef dbl = double
*)

(*
abstype exp // = ptr
*)
datatype
exp =
| EXPcst of (double)
| EXPadd of (exp, exp)
| EXPsub of (exp, exp)
| EXPmul of (exp, exp)
| EXPdiv of (exp, exp)
//
(* ****** ****** *)
//
extern
fun
int2exp : int -> exp
implement
int2exp(i0) = EXPcst(int2double(i0))
//
(* ****** ****** *)
//
#define i2e int2exp
//
(* ****** ****** *)

#define EPSILON 0.000001

(* ****** ****** *)
//
extern
fun
eval : exp -> double
//
extern
fun
add_exp_exp : (exp, exp) -> exp
extern
fun
sub_exp_exp : (exp, exp) -> exp
extern
fun
mul_exp_exp : (exp, exp) -> exp
extern
fun
div_exp_exp : (exp, exp) -> exp
//
overload + with add_exp_exp
overload - with sub_exp_exp
overload * with mul_exp_exp
overload / with div_exp_exp
//
(* ****** ****** *)
//
fun
is_0(x: exp) = abs(eval(x)) < EPSILON
fun
is_24(x: exp) = abs(eval(x)-24.0) < EPSILON
//
(* ****** ****** *)
//
extern
fun
arithops(x: exp, y: exp): list0(exp)
//
(* ****** ****** *)
//
implement
eval(e0) =
(
case+ e0 of
| EXPcst(x) => x
| EXPadd(e1, e2) => eval(e1) + eval(e2)
| EXPsub(e1, e2) => eval(e1) - eval(e2)
| EXPmul(e1, e2) => eval(e1) * eval(e2)
| EXPdiv(e1, e2) => eval(e1) / eval(e2)
)
//
(* ****** ****** *)

implement
add_exp_exp(e1, e2) = EXPadd(e1, e2)
implement
sub_exp_exp(e1, e2) = EXPsub(e1, e2)
implement
mul_exp_exp(e1, e2) = EXPmul(e1, e2)
implement
div_exp_exp(e1, e2) = EXPdiv(e1, e2)

(* ****** ****** *)
//
implement
arithops(x, y) =
  reverse(res) where
{
  val res = nil0()
  val res = cons0(x + y, res)
  val res = cons0(x - y, res)
  val res = cons0(y - x, res)
  val res = cons0(x * y, res)
  val res = (if is_0(y) then res else cons0(x / y, res)): list0(exp)
  val res = (if is_0(x) then res else cons0(y / x, res)): list0(exp)
}
//
(* ****** ****** *)
//
extern
fun
solve_one(list0(exp)): stream(list0(exp))
extern
fun
solve_ones(stream(list0(exp))): stream(list0(exp))
//
(* ****** ****** *)
//
implement
solve_one
  (xs) = let
//
val
x_y_zs_list =
  choose2(xs)
//
in
//
stream_concat
(
x_y_zs_list.map
(
TYPE
{
stream(list0(exp))
}(*TYPE*)
)
(
lam
(
  $tup(x, y, zs)
) =>
  stream_make_list0
  (
    (arithops(x, y)).map(TYPE{list0(exp)})(lam z => cons0(z, zs))
  )(* stream_make_list0 *)
)(* x_y_zs_list.map *)
)
//
end // end of [solve_one]
//
(* ****** ****** *)
//
implement
solve_ones =
lam(xss) =>
stream_concat
(
  (xss).map(TYPE{stream(list0(exp))})(lam xs => solve_one(xs))
) (* end of [solve_ones] *)
//
(* ****** ****** *)
//
extern
fun
Game24_play
(
  n1: int, n2: int, n3: int, n4: int
) : stream(exp) = "mac#"
//
(* ****** ****** *)

implement
Game24_play
(
  n1, n2, n3, n4
) = let
//
  val xs = nil0()
  val xs = cons0(i2e(n1), xs)
  val xs = cons0(i2e(n2), xs)
  val xs = cons0(i2e(n3), xs)
  val xs = cons0(i2e(n4), xs)
//
  val xss = solve_one(xs)
  val xss = solve_ones(xss)
  val xss = solve_ones(xss)
in
//
stream_map_cloref
{list0(exp)}{exp}
(
//
xss.filter()(lam(xs) => let val-sing0(x) = xs in is_24(x) end), lam(xs) => xs.head()
//
) (* end of [stream_map_cloref] *)
//
end // end of [Game24_play]

(* ****** ****** *)
//
extern
fun
exp2string : exp -> string
implement
exp2string(e0) =
(
case+ e0 of
| EXPcst(x) => String(x)
| EXPadd(e1, e2) => "(" + exp2string(e1) + " + " + exp2string(e2) + ")"
| EXPsub(e1, e2) => "(" + exp2string(e1) + " - " + exp2string(e2) + ")"
| EXPmul(e1, e2) => "(" + exp2string(e1) + " * " + exp2string(e2) + ")"
| EXPdiv(e1, e2) => "(" + exp2string(e1) + " / " + exp2string(e2) + ")"
)
//
(* ****** ****** *)
//
extern
fun
Game24_play2
(
  n1: int, n2: int, n3: int, n4: int
) : string = "mac#"
//
implement
Game24_play2
(
  n1, n2, n3, n4
) = let
  val es = Game24_play(n1, n2, n3, n4)
  val es = es.map(TYPE{string})(lam e => exp2string(e))
  val es = stream2list(es)
in
//
case+ es of
| list_nil _ => "There is no solution!\n"
| list_cons _ => JSarray_join_sep(JSarray_make_list(es), "\n")
//
end // end of [Game24_play2]
//
(* ****** ****** *)

(* end of [Game-of-24_sol.dats] *)
