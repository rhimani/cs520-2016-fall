(*
** Assignment 5
** It is due Wednesday, the 12th of October, 2016
*)
(* ****** ****** *)
//
// HX: No use of library functions is allowed!!!
//
(* ****** ****** *)
//
// HX: 10 points
//
extern
fun{a:t@ype}
mylist_append
  {m,n:int}
  (list(INV(a), m), list(a, n)): list(a, m+n)
//
(* ****** ****** *)
//
// HX: 10 points
//
extern
fun{a:t@ype}
mylist_reverse
  {n:int}(xs: list(INV(a), n)): list(a, n)
//
extern
fun{a:t@ype}
mylist_revappend
  {m,n:int}
  (list(INV(a), m), list(a, n)): list(a, m+n)
//
(* ****** ****** *)
//
implement{a}
mylist_reverse(xs) = mylist_revappend<a>(xs, nil)
//  
(* ****** ****** *)
//
// HX: 10 points
//
extern
fun
{a,b:t@ype}
mylist_zip
  {n:int}(list(INV(a), n), list(INV(b), n)): list(@(a, b), n)
//
(* ****** ****** *)

(* end of [assign5.dats] *)
