(*
** Solution to Draw_a_sphere.dats
*)

(* ****** ****** *)
//
#include
"share/atspre_define.hats" // defines some names
#include
"share/atspre_staload.hats" // for targeting C
#include
"share/HATS/atspre_staload_libats_ML.hats" // for ...
#include
"share/HATS/atslib_staload_libats_libc.hats" // for libc
//
(* ****** ****** *)

#include "./../Draw_a_sphere.dats"

(* ****** ****** *)

(* end of [Draw_a_sphere_sol.dats] *)
