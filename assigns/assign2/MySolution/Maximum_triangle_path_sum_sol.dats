(*
** Solution to
** Maximum_triangle_path_sum.dats
*)

(* ****** ****** *)
//
#include
"share/atspre_define.hats" // defines some names
#include
"share/atspre_staload.hats" // for targeting C
#include
"share/HATS/atspre_staload_libats_ML.hats" // for ...
#include
"share/HATS/atslib_staload_libats_libc.hats" // for libc
//
(* ****** ****** *)

#include "./../Maximum_triangle_path_sum.dats"

(* ****** ****** *)

(* end of [Maximum_triangle_path_sum.dats] *)
