(*
** Assignment 3
** It is due Wednesday,
** the 28th of September, 2016
*)

(* ****** ****** *)
//
// HX: please do typechecking as follows:
//
// patscc -tcats assign3_sol_sample.dats
//
(* ****** ****** *)

#include "./../assign3.dats"

(* ****** ****** *)
//
extern
prfun
DistLaw_disjconj
  {A,B,C:prop}
  (pf: A || (B && C)): (A || B) && (A || C)
// end of [DistLaw_disjconj]
//
(* ****** ****** *)

primplement
DistLaw_disjconj
  {A,B,C}(pf) =
(
case pf of
| disj_intr_l(pfA) =>
  conj_intr(disj_intr_l(pfA), disj_intr_l(pfA))
| disj_intr_r(pfBC) => let
    prval pfB = conj_elim_l(pfBC)
    prval pfC = conj_elim_r(pfBC)
  in
    conj_intr(disj_intr_r(pfB), disj_intr_r(pfC))
  end // end of [disj_intr_r]
) (* DistLaw_disjconj *)

(* ****** ****** *)
//
primplement
LEM{A}((*void*)) =
  LDN(neg_intr(lam(pfN: ~(A||(~A))) => neg_elim(pfN, disj_intr_r(neg_intr(lam(pfA: A) => neg_elim(pfN, disj_intr_l(pfA)))))))
//
(* ****** ****** *)

implement main0 () = ()

(* ****** ****** *)

(* end of [assign3_sol_sample.dats] *)
