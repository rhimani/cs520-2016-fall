(* ****** ****** *)

(*
** Breadth-first listing
*)

(* ****** ****** *)
//
// HX: 20 bonus points
//
(* ****** ****** *)
//
extern
fun
lsdir_bfst(path: string): stream(string)
//
(* ****** ****** *)
//
extern
fun
lsdir_bfst_vt(path: string): stream_vt(string)
//
(* ****** ****** *)

(* end of [lsdir_bfst.dats] *)
