(*
** Depth-first listing
*)

(* ****** ****** *)
//
// HX: 20 points
//
(* ****** ****** *)
//
extern
fun
lsdir_dfst(path: string): stream(string)
//
(* ****** ****** *)
//
extern
fun
lsdir_dfst_vt(path: string): stream_vt(string)
//
(* ****** ****** *)

(* end of [lsdir_dfst.dats] *)
